/**
 * Global utility function adding event handlers with retro-compatability.
 */
function addCustomEvents(el, eventName, handler) {
    if (el.addEventListener) {
        el.addEventListener(eventName, handler);
    } else {
        el.attachEvent('on' + eventName, function(){
            handler.call(el);
        });
    }
}

/**
 * Global utility function string trim functionality.
 */
if(typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

/**
 * Global utility function for fade in animation.
 */
function fadeIn(elem, ms) {
    if(!elem) return;

    elem.style.opacity = 0;
    elem.style.filter = 'alpha(opacity=0)';
    elem.style.display = 'inline-block';
    elem.style.visibility = 'visible';

    if(ms) {
        var opacity = 0;
        var timer = setInterval(function() {
            opacity += 50 / ms;
            if(opacity >= 1)
            {
                clearInterval(timer);
                opacity = 1;
            }
            elem.style.opacity = opacity;
            elem.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
        }, 50 );
    } else {
        elem.style.opacity = 1;
        elem.style.filter = 'alpha(opacity=1)';
    }
}

/**
 * Global utility function for fade out animation.
 */
function fadeOut(elem, ms) {
    if(!elem) return;

    if(ms) {
        var opacity = 1;
        var timer = setInterval(function() {
            opacity -= 50 / ms;
            if(opacity <= 0) {
                clearInterval(timer);
                opacity = 0;
                elem.style.display = 'none';
                elem.style.visibility = 'hidden';
            }
            elem.style.opacity = opacity;
            elem.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
        }, 50 );
    } else {
        elem.style.opacity = 0;
        elem.style.filter = 'alpha(opacity=0)';
        elem.style.display = 'none';
        elem.style.visibility = 'hidden';
    }
}

/**
 * Global function call to check if user is in desktop or mobile version.
 * Sets to variable for later use.
 */
window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    var ieMobile = navigator.userAgent.match(/iemobile/i);
    if (ieMobile) check = false;
    return check;
};

var SimplePay = SimplePay || {};
var spIsMobile = window.mobilecheck();

/**
 * SimplePay js api for merchants call.
 * Implements functions to configure and open gateway frontend iframe/tab.
 */
(function (SimplePay) {
    "use strict";

    var clientFunction,
        clientCloseFunction,
        tokenHeadValidation = 'trans',
        tkHeadValidation = 'tk',
        targetOpenedWindow,
        frameValidation,
        frameOpened = false,
        secureTrigered = false,
        successCloseOperation,
        secureOperationTimeOut,
        simplePayGatewayDomain = 'https://checkout.simplepay.ng',
        sourceUrl = simplePayGatewayDomain + '/v1/merchants/inner/',
        loadedFrame = false,
        mobileOpenValidationInterval;

    /**
     * Attach event listener for postMessage results.
     * Call is diferent to support old IE versions.
     * See workflow documentation for more on this.
     */
    if (window.addEventListener){
        addEventListener('message', listener, false)
    } else {
        attachEvent('onmessage', listener)
    }

    /**
     * Handler for removing overlay div showing impropoer configuration..
     */
    function removeErrorOverlay() {
        var element = document.getElementById('spOverlayError');
        element.parentNode.removeChild(element);
        if (clientCloseFunction) clientCloseFunction.call(null);
    }

    /**
     * Listener for postMessage comming from other iframes/tabs.
     * See workflow documentation for more on this.
     * @param {object} event Event with host origin and data message.
     */
    function listener(event){
        if (event.origin === simplePayGatewayDomain) {
            var secureEl = document.getElementById('simplePay3DSecureFrame');
            var simplePayFrame = document.getElementById('simplePayFrame');
            var simplePayFrameContainer = document.getElementById('simplePayFrameContainer');
            if (event.data === 'close3DSecure') {
                if (secureOperationTimeOut) {
                    window.clearTimeout(secureOperationTimeOut);
                }
                setTimeout(function(){
                    secureEl.parentNode.removeChild(secureEl);
                }, 500);
            }
            else if (event.data === 'close') {
                if (secureOperationTimeOut) {
                    window.clearTimeout(secureOperationTimeOut);
                }
                fadeOut(document.getElementById('simplePayFrameContainer'), 500);
                setTimeout(function(){
                    if (spIsMobile) {
                        targetOpenedWindow.close();
                    } else {
                        simplePayFrame.style.display = 'none';
                        simplePayFrameContainer.style.display = 'none';
                        frames['simplePayFrame'].postMessage('resetIFrame', simplePayGatewayDomain);
                    }
                }, 500);
                if (clientCloseFunction) clientCloseFunction.call(null);
                frameOpened = false;
            } else {
                try{
                    var parsedJson = JSON.parse(event.data);
                }catch(e){
                    // If it's not something sent by us
                    return;
                }
                if ('token' in parsedJson) {
                    if (secureOperationTimeOut) {
                        window.clearTimeout(secureOperationTimeOut);
                    }
                    if (parseInt(parsedJson['frame']) !== frameValidation) return;
                    if (secureTrigered) {
                        secureTrigered = false;
                        if (secureEl) {
                            secureEl.parentNode.removeChild(secureEl);
                        }
                    }
                    if ('fast_close' in parsedJson && parsedJson['token'].indexOf(tokenHeadValidation) === 0) {
                        if (successCloseOperation) {
                            window.clearTimeout(successCloseOperation);
                        }
                        clientFunction.call(null, parsedJson['token']);
                        setTimeout(function () {
                            if (spIsMobile) {
                                targetOpenedWindow.close();
                            } else {
                                simplePayFrame.style.display = 'none';
                                simplePayFrameContainer.style.display = 'none';
                                frames['simplePayFrame'].postMessage('resetIFrame', simplePayGatewayDomain);
                            }
                        }, 2000);
                    } else {
                        if (parsedJson['token'].indexOf(tokenHeadValidation) === 0 || parsedJson['token'].indexOf(tkHeadValidation) === 0) {
                            clientFunction.call(null, parsedJson['token']);
                            successCloseOperation = setTimeout(function(){
                                if (spIsMobile) {
                                    targetOpenedWindow.close();
                                } else {
                                    simplePayFrame.style.display = 'none';
                                    simplePayFrameContainer.style.display = 'none';
                                    frames['simplePayFrame'].postMessage('resetIFrame', simplePayGatewayDomain);
                                }
                            }, 2000);
                        }
                    }
                    frameOpened = false;
                }
                if ('redirect' in parsedJson) {
                    secureTrigered = true;
                    if (!spIsMobile) {
                        var simplePay3DSecureFrame = document.createElement('div');
                        simplePay3DSecureFrame.id = 'simplePay3DSecureFrame';
                        simplePay3DSecureFrame.style.display = 'none';
                        document.body.appendChild(simplePay3DSecureFrame);

                        var simplePay3DSIFrame = document.createElement('iframe');
                        simplePay3DSIFrame.id = 'simplePay3DSIFrame';
                        simplePay3DSIFrame.src = parsedJson['redirect'];
                        simplePay3DSIFrame.setAttribute('name', 'simplePay3DIFrame');
                        simplePay3DSIFrame.setAttribute('frameborder', '0');
                        simplePay3DSIFrame.setAttribute('scrolling', 'no');
                        simplePay3DSIFrame.style.position = 'absolute';
                        simplePay3DSIFrame.style.top = '50%';
                        simplePay3DSIFrame.style.left = '50%';
                        simplePay3DSIFrame.style.width = '502px';
                        simplePay3DSIFrame.style.height = '516px';
                        simplePay3DSIFrame.style.zIndex = 99999;
                        simplePay3DSIFrame.style.backgroundColor = '#F1EFEF';
                        simplePay3DSIFrame.style.marginLeft = '-251px';
                        simplePay3DSIFrame.style.marginTop = '-258px';
                        simplePay3DSecureFrame.appendChild(simplePay3DSIFrame);

                        addCustomEvents(simplePay3DSIFrame, 'load', function() {
                            simplePay3DSIFrame.style.display = 'block';
                            fadeIn(simplePay3DSecureFrame, 500);
                            simplePay3DSIFrame.focus();
                        });

                        secureOperationTimeOut = setTimeout(checkForTimeout, 180000);
                    }
                }
            }
        }
    }

    /**
     * Validates url provided by merchants.
     * @param {string} strUrl Url to validate.
     * @return {object||bool} Valid or invalid url path.
     */
    function isValidUrl(strUrl) {
        var pattern = new RegExp('^(https?:\\/\\/)?((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|((\\d{1,3}\\.){3}\\d{1,3}))(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*(\\?[;&a-z\\d%_.~+=-]*)?(\\#[-a-z\\d_]*)?$','i');
        return pattern.test(strUrl);
    }

    /**
     * If function is called, display timeout information to user.
     */
    function checkForTimeout() {
        // stop requests
        window.stop();
        // send error to child
        if (secureTrigered) {
            var secureEl = document.getElementById('simplePay3DSecureFrame');
            secureTrigered = false;
            secureEl.parentNode.removeChild(secureEl);
        }
        // catch frame by name in order to get the right one
        frames['simplePayFrame'].postMessage('timeout', simplePayGatewayDomain);
        frameOpened = false;
    }

    /**
     * Creates and shows an improper configuration message popup.
     */
    function showImproperConfig (error_type, error_message, error_submessage) {
        frameOpened = false;

        if(targetOpenedWindow) {
            targetOpenedWindow.close()
        }
        var errorDiv = document.createElement('div');
        errorDiv.id = 'spOverlayError';
        errorDiv.style.position = 'fixed';
        errorDiv.style.left = 0;
        errorDiv.style.top = 0;
        errorDiv.style.width = '100%';
        errorDiv.style.height = '100%';
        errorDiv.style.textAlign = 'center';
        errorDiv.style.zIndex = 99998;
        errorDiv.style.backgroundColor = 'rgba(0,0,0,0.6)';
        document.body.appendChild(errorDiv);

        var errorInnerDiv = document.createElement('div');
        errorInnerDiv.id = 'spOverlayInnerError';
        errorInnerDiv.style.fontFamily = '"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif';
        errorInnerDiv.style.left = '50%';
        errorInnerDiv.style.top = '50%';
        errorInnerDiv.style.borderRadius = '5px';
        errorInnerDiv.style.margin = '100px auto';
        errorInnerDiv.style.backgroundColor = '#F27474';
        errorInnerDiv.style.width = '300px';
        errorInnerDiv.style.textAlign = 'center';
        errorInnerDiv.style.padding = '15px';
        errorInnerDiv.style.overflow = 'hidden';
        errorDiv.appendChild(errorInnerDiv);

        var spErrorH2 = document.createElement('h2');
        spErrorH2.id = 'spErrorH2';
        spErrorH2.style.color = '#FFF';
        spErrorH2.style.padding = '20px 0 0 0';
        spErrorH2.innerHTML = error_type;
        errorInnerDiv.appendChild(spErrorH2);

        if (error_message) {
            var spErrorE = document.createElement('p');
            spErrorE.id = 'spErrorE';
            spErrorE.style.color = '#FFF';
            spErrorE.style.padding = '10px 0 0 0';
            spErrorE.innerHTML = error_message;
            errorInnerDiv.appendChild(spErrorE);
        }

        if (error_submessage) {
            var spErrorP = document.createElement('p');
            spErrorP.id = 'spErrorP';
            spErrorP.style.color = '#FFF';
            spErrorP.style.padding = '10px 0 0 0';
            spErrorP.innerHTML = error_submessage;
            errorInnerDiv.appendChild(spErrorP);
        }

        var spErrorBtnClosure = document.createElement('button');
        spErrorBtnClosure.id = 'spErrorBtnClosure';
        spErrorBtnClosure.style.color = '#F27474';
        spErrorBtnClosure.style.backgroundColor = '#FFF';
        spErrorBtnClosure.style.borderRadius = '5px';
        spErrorBtnClosure.style.padding = '10px 32px';
        spErrorBtnClosure.style.border = 'none';
        spErrorBtnClosure.style.margin = '26px 5px 0 5px';
        spErrorBtnClosure.style.fontSize = '17px';
        spErrorBtnClosure.style.fontWeight = '500';
        spErrorBtnClosure.style.cursor = 'pointer';
        spErrorBtnClosure.style.outline = 'none';
        spErrorBtnClosure.innerHTML = 'OK';
        errorInnerDiv.appendChild(spErrorBtnClosure);

        // Add event to improper configuration box ok button
        addCustomEvents(spErrorBtnClosure, 'click', removeErrorOverlay);
    }

    /**
     * Shows SimplePay IFrame.
     * @param {string} paymentType Payment type choosed by merchant.
     * @param {object} options Options from open method.
     * @param {object} configuredOptions Options gathered from configure.
     */
    function showSPPayment (paymentType, options, configuredOptions) {

        // Create a variable for validations in iframes. See workflow documentation for more on this.
        if (!Date.now) {
            frameValidation = new Date().getTime();
        } else {
            frameValidation = Date.now();
        }

        if (spIsMobile) {
            // Build an url query for get calls
            var queryUrl = 'payment_type=' + paymentType;
            for (var param in options) {
                queryUrl += '&' + param + '=' + String(options[param]).trim();
            }

            // Merge options dict with configuration options dict
            options = mergeOptions(options, configuredOptions);

            if (options['image']) {
                queryUrl += '&image=' + encodeURIComponent(String(options['image']).trim());
            }

            queryUrl += '&key=' + String(options['key']).trim();
            queryUrl += '&frameVal=' + frameValidation;

            // Add merchants host for proper and filtered postMessage communication.
            queryUrl += '&host=' + location.protocol + '//' + window.location.hostname + (window.location.port ? ':'+window.location.port: '');

            // Replace '+' sign on url query by its hex value
            queryUrl = queryUrl.replace(/\+/g, '%2B');
            queryUrl = queryUrl.replace(/#/g, '%23');

            // Check if tab was already opened. If so, focus tab, else open.
            if(!targetOpenedWindow || targetOpenedWindow.closed) {
                // if it hits here, it will probably generate a pop-up block
                targetOpenedWindow = window.open(
                    sourceUrl + 'mobile/?' + queryUrl,
                    '_blank'
                );
            } else {
                targetOpenedWindow.location.replace(sourceUrl + 'mobile/?' + queryUrl);
                targetOpenedWindow.focus();
            }
            mobileOpenValidationInterval = setInterval(function() {
                if(!targetOpenedWindow || targetOpenedWindow.closed) {
                    clearInterval(mobileOpenValidationInterval);
                    if (clientCloseFunction) clientCloseFunction.call(null);
                    frameOpened = false;
                }
            }, 1000);
        } else {
            // Trim all options before merging objects
            for (var param in options) {
                options[param] = String(options[param]).trim();
            }

            // Merge options dict with configuration options dict
            options = mergeOptions(options, configuredOptions);

            options['token'] = true;
            options['close'] = true;
            options['payment_type'] = paymentType;
            options['frameVal'] = frameValidation;
            options['host'] = location.protocol + '//' + window.location.hostname + (window.location.port ? ':'+window.location.port: '');

            var intervalDispatcher;

            if (loadedFrame) {
                frames['simplePayFrame'].postMessage(JSON.stringify(options), simplePayGatewayDomain);
            } else {
                intervalDispatcher = setInterval(function() {
                    if (loadedFrame) {
                        frames['simplePayFrame'].postMessage(JSON.stringify(options), simplePayGatewayDomain);
                        clearInterval(intervalDispatcher);
                        loadedFrame = false;
                    }
                }, 1000);
            }
            var simplePayFrame = document.getElementById('simplePayFrame');
            var simplePayFrameContainer = document.getElementById('simplePayFrameContainer');
            simplePayFrame.style.display = 'block';
            fadeIn(simplePayFrameContainer, 200);
            simplePayFrame.focus();
        }
    }

    /**
     * Merges two dict objects into one.
     * @param {object} obj1 First dict to be merged.
     * @param {object} obj2 Second dict to be merged.
     * @return {object} obj3 Merge results from obj1 and obj2.
     */
    function mergeOptions (obj1,obj2){
        var obj3 = {};
        for (var obj1Attr in obj1) { obj3[obj1Attr] = obj1[obj1Attr]; }
        for (var obj2Attr in obj2) { obj3[obj2Attr] = obj2[obj2Attr]; }
        return obj3;
    }

    /**
     * SimplePay PreAuth payment type.
     * @const {string}
     */
    SimplePay.REMEMBER = 'remember';

    /**
     * SimplePay Checkout payment type.
     * @const {string}
     */
    SimplePay.CHECKOUT = 'checkout';

    /**
     * SimplePay Escrow payment type.
     * @const {string}
     */
    SimplePay.ESCROW = 'escrow';

    /**
     * SimplePay PreAuth payment type.
     * @const {string}
     */
    SimplePay.PREAUTH = 'preauth';

    /**
     * Creates an Iframe with preload template
     */
    function createIframeTemplate() {
        if (!spIsMobile) {
            if (!simplePayFrame && !simplePayFrameContainer) {

                var simplePayFrameContainer = document.createElement('div');
                simplePayFrameContainer.id = 'simplePayFrameContainer';
                simplePayFrameContainer.style.display = 'none';
                document.body.appendChild(simplePayFrameContainer);

                var simplePayFrame = document.createElement('iframe');
                simplePayFrame.id = 'simplePayFrame';
                simplePayFrame.src = sourceUrl + 'desktop/';
                simplePayFrame.setAttribute('name', 'simplePayFrame');
                simplePayFrame.setAttribute('frameborder', '0');
                simplePayFrame.setAttribute('scrolling', 'no');
                simplePayFrame.style.position = 'fixed';
                simplePayFrame.style.top = 0;
                simplePayFrame.style.left = 0;
                simplePayFrame.style.width = '100%';
                simplePayFrame.style.height = '100%';
                simplePayFrame.style.zIndex = 99999;
                simplePayFrame.style.display = 'none';
                simplePayFrameContainer.appendChild(simplePayFrame);
            }

            addCustomEvents(simplePayFrame, 'load', function () {
                loadedFrame = true
            });
        }
    }

    /**
    * Preloads IFrame on desktop environment
    */
    if (document.body) {
        createIframeTemplate();
    } else {
        window.onload = function() {
            createIframeTemplate();
        };
    }

    /**
     * Configures amount to be sent to API.
     * @param {String|int} amount Amount in normal denomination.
     * @return {String} lower_amount Amount in lower denomination.
     */
    SimplePay.amountToLower = SimplePay.amount_to_lower = (function (amount) {
        var strAmountList = String(amount).trim().split('.');
        var decimalPlaces = strAmountList[1] === undefined ? 0 : strAmountList[1].length;
        var lowerAmount = strAmountList[0];

        if (decimalPlaces === 0) {
            lowerAmount += '00';
        } else if (decimalPlaces === 1) {
            lowerAmount += strAmountList[1] + '0';
        } else if (decimalPlaces === 2) {
            lowerAmount += strAmountList[1];
        } else if (decimalPlaces > 2) {
            lowerAmount += strAmountList[1].substring(0, 2);
        }

        strAmountList[0].indexOf('0') === 0 ? lowerAmount = String(parseInt(lowerAmount)) : void 0;

        return lowerAmount;
    });

    /**
     * Configures SimplePay js api.
     * @param {object} options Dict with options for configuration.
     * @return {object|boolean} open Function to open gateway frontend's iframe/tab.
     * @example SimplePay.configure({token: callbackF, key: 'api_key_123', image: 'url/path/to/image'});
     */
    SimplePay.configure = (function (options) {
        // Start configuredOptions dict for future merging of options
        var configuredOptions = options;

        // Check if token callback function is in the options dict
        if (options['token']) {
            clientFunction = options['token'];
        } else {
            showImproperConfig('Improper Configuration', 'Invalid callback.', 'Please check with the support team!');
            return false;
        }

        if (options['close']) {
            clientCloseFunction = options['close'];
        }

        if (options['key']) {
            options['key'] = String(options['key']).trim();
        } else {
            showImproperConfig('Improper Configuration', 'Missing API key.', 'Please check with the support team!');
            return false;
        }

        // Validate image url path
        if (options['image']) {
            options['image'] = String(options['image']).trim();
            var isValidPath = isValidUrl(options['image']);
            if (!isValidPath) {
                delete configuredOptions['image'];
            }
        }
        // Validate plataform and make configure call to Segment
        if (!('platform' in options) || !options['platform']) {
            options['platform'] = 'Web';
        }

        return {
            /**
             * Open SimplePay gateway frontend's iframe/tab.
             * @param {string} paymentType SimplePay payment type.
             * @param {object} options Dict with options for passing arguments to iframe/tab.
             * @example handler.open(SimplePay.ESCROW, {email: '', phone: '', desc: '', address: '', postal: '', ...});
             * @return {boolean}
             */
            open: function(paymentType, options) {
                if (navigator.userAgent.match(/Opera.+Opera Mini/i) || navigator.userAgent.match(/OPR.+Mobile/i)) {
                    showImproperConfig('Opera Mini Not Supported', 'We currently do not support Opera Mini Web Browser as it makes our payment gateway insecure.', 'Please use a different browser or check in with our support!');
                    return false;
                }

                if (!frameOpened) {
                    frameOpened = true;
                    // If on mobile, open upfront the tab in which the html is going to be the sorce
                    if (spIsMobile) {
                        var additionalInfo = '';
                        targetOpenedWindow = window.open(
                            '',
                            '_blank'
                        );

                        var spinnerImageSource = simplePayGatewayDomain + '/static/v1/gif/spinner.gif';

                        if (navigator.userAgent.indexOf('Opera Mini') !== -1) {
                            var operaImageSource = simplePayGatewayDomain + '/static/v1/img/opera/opera_mini_types.png';
                            additionalInfo =  '<br>' +
                                '<br>' +
                                '<br>' +
                                '<br>' +
                                '<img src="' + operaImageSource + '" style="width: 180px; margin-left: 40%;">' +
                                '<p style="color: #77439e; text-align: center; font-size: 24px">' +
                                '<b>NOTE: </b>' +
                                'On Opera Mini, please use <b style="color: #569ce3;">High Mode</b> (blue option)' +
                                '</p>';
                        }

                        targetOpenedWindow.document.write('<html style="font-family: Helvetica, Arial, sans-serif;">' +
                            '<body style="background-color: #f5f5f5;">' +
                            '<p style="color: #77439e; text-align: center; font-size: 82px">' +
                            '<b>SIMPLEPAY</b>' +
                            '</p>' +
                            '<br>' +
                            '<div style="width: 100%">' +
                            '<img style="width: 200px;height: 200px;margin-left: 40%;" src="' + spinnerImageSource + '">' +
                            '</div>' +
                            additionalInfo +
                            '</body>' +
                            '</html>');
                    }

                    showSPPayment(paymentType, options, configuredOptions);

                    return true;
                }
            }
        }
    });

}) (SimplePay);
