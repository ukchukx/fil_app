import * as actions from './actions';
import * as getters from './getters';
import { FETCH_TAGS, SAVE_TAG, DELETE_TAG } from './mutation-types';

const initialState = {
  tags: []
};

/* eslint-disable no-param-reassign */
const mutations = {
  [FETCH_TAGS](state, tags) {
    state.tags = tags;
  },
  [SAVE_TAG](state, tag) {
    const index = state.tags.findIndex(t => t.id === tag.id);

    if (index !== -1) {
      state.tags.splice(index, 1, tag);
    } else {
      state.tags.push(tag);
    }
  },
  [DELETE_TAG](state, id) {
    state.tags = state.tags.filter(r => r.id !== id);
  }
};

export default {
  state: Object.assign({}, initialState),
  actions,
  getters,
  mutations
};
