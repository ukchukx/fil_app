export const FETCH_TAGS = 'tags/fetch_tags';
export const SAVE_TAG = 'tags/save_tag';
export const DELETE_TAG = 'tags/delete_tag';
