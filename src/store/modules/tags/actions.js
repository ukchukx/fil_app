import { save, fetch, doDelete } from '@/store/common';
import { APP_CONSTANTS, canRefresh } from '@/constants';
import { FETCH_TAGS, SAVE_TAG, DELETE_TAG } from './mutation-types';

const tagApi = 'tags';

/* eslint-disable no-unused-vars */

export function fetchTags({ commit, getters, dispatch }) { 
  if (!canRefresh(getters.getLastRefreshTimestamp.tags)) { 
    return { success: APP_CONSTANTS.OK, data: getters.getTags }; 
  } 
 
  dispatch('registerLastRefreshTimestamp', 'tags'); 
  return fetch(tagApi)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(FETCH_TAGS, data);
      }
      return { success, data };
    });
}

export function saveTag({ commit }, tag) {
  return save(tagApi, tag)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_TAG, data);
      }
      return { success, data };
    });
}

export function deleteTag({ commit }, id) {
  return doDelete(`${tagApi}/${id}`)
    .then(({ success }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(DELETE_TAG, id);
      }
      return { success };
    });
}

