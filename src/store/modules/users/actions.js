import Vue from 'vue';
import { save, fetch, doDelete, savePayment } from '@/store/common';
import { APP_CONSTANTS } from '@/constants';
import { round } from '@/filters';

import {
  FETCH_USERS,
  DELETE_USER,
  SAVE_USER,
  SET_AUTH_USER,
  REMOVE_AUTH_USER
} from './mutation-types';

import { FETCH_ORDERS } from '../orders/mutation-types';

const userApi = 'users';
const authApi = 'auth';


/* eslint-disable no-unused-vars */

export function fetchUsers({ commit }) {
  return fetch(userApi)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(FETCH_USERS, data);
      }
    });
}

export function saveUser({ commit, getters }, user) {
  return save(userApi, user)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_USER, data);
        // Update auth user
        const authUser = getters.getUser;
        if (authUser.id === data.id) {
          commit(SET_AUTH_USER, Object.assign(authUser, data));
        }
      }
      return { success, data };
    });
}

export function updateWallet({ commit, getters }, { id, amount, action }) {
  const user = getters.getUsers.find(u => u.id === id);
  const currentUser = getters.getUser;
  let newBalance;

  switch (action) {
    case 'increment':
      newBalance = round(user.wallet + amount);
      if (currentUser.id === id) {
        commit(SET_AUTH_USER, Object.assign({ ...currentUser }, { wallet: newBalance }));
      }
      commit(SAVE_USER, Object.assign({ ...user }, { wallet: newBalance }));
      break;
    case 'decrement':
      newBalance = round(Math.max(0, user.wallet - amount));
      if (currentUser.id === id) {
        commit(SET_AUTH_USER, Object.assign({ ...currentUser }, { wallet: newBalance }));
      }
      commit(SAVE_USER, Object.assign({ ...user }, { wallet: newBalance }));
      break;
    default: return { success: APP_CONSTANTS.FAILED };
  }

  return { success: APP_CONSTANTS.OK };
}

export function refund({ dispatch }, { user_id, amount }) {
  return Vue.http.post(`${userApi}/actions/refund`, { user_id, amount })
    .then((response) => {
      dispatch('updateWallet', { id: user_id, amount, action: 'increment' });
      return { success: APP_CONSTANTS.OK };
    });
}

export function fundWallet({ commit, getters }, payload) {
  payload.user_id = payload.id;
  const paymentPayload = {
    amount: payload.amount,
    processor: payload.processor,
    user_id: payload.id,
    ref: payload.ref
  };

  return savePayment(paymentPayload)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        payload.token = data.token;
      }
      return Vue.http.post(`${userApi}/actions/fund-wallet`, payload)
        .then((response) => {
          commit(SAVE_USER, response.body.data);
          // Update logged in user if id is theirs
          const user = getters.getUser;
          if (user.id === response.body.data.id) {
            commit(SET_AUTH_USER, Object.assign(user, response.body.data));
          }
          return APP_CONSTANTS.OK;
        }, error => APP_CONSTANTS.FAILED);
    });
}

export function deleteUser({ commit }, id) {
  return doDelete(`${userApi}/${id}`)
    .then(({ success }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(DELETE_USER, id);
      }
      return { success };
    });
}

export function authenticate({ commit }, details) {
  const api = details.first_name ? `${authApi}/signup` : `${authApi}/authenticate`;
  return Vue.http.post(api, details)
  .then((response) => {
    commit(SET_AUTH_USER, response.body.data);

    delete response.body.data.token;
    commit(SAVE_USER, response.body.data);
    return APP_CONSTANTS.OK;
  }, error => APP_CONSTANTS.FAILED);
}

export function forgotPassword(ctx, data) {
  return Vue.http.post(`${authApi}/forgot-password`, data)
    .then(response => Object.assign({}, { success: APP_CONSTANTS.OK, data: response.body.data })
    , error => Object.assign({}, { success: APP_CONSTANTS.FAILED, data: error.body.data }));
}

export function resetPassword({ commit }, data) {
  return Vue.http.post(`${authApi}/reset-password`, data)
    .then((response) => {
      commit(SET_AUTH_USER, response.body.data);
      commit(SAVE_USER, response.body.data);
      return APP_CONSTANTS.OK;
    }, error => APP_CONSTANTS.FAILED);
}

export function signout({ commit }) {
  commit(REMOVE_AUTH_USER);
  commit(FETCH_USERS, []);
  commit(FETCH_ORDERS, []);
}

