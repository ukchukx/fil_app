export const FETCH_ORDERS = 'orders/fetch_orders';
export const SAVE_ORDER = 'orders/save_order';
export const SAVE_WISHLIST = 'orders/save_wishlist';
export const DELETE_WISHLIST = 'orders/delete_wishlist';
export const REPLACE_WISHLIST = 'orders/replace_wishlist';
export const REPLACE_WISHLIST_ITEM = 'orders/replace_wishlist_item';
export const DELETE_WISHLIST_ITEM = 'orders/delete_wishlist_item';
