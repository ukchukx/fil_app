import { save, fetch, savePayment } from '@/store/common';
import { APP_CONSTANTS } from '@/constants';
import { FETCH_ORDERS,
  SAVE_ORDER,
  SAVE_WISHLIST,
  REPLACE_WISHLIST_ITEM,
  DELETE_WISHLIST_ITEM,
  DELETE_WISHLIST
} from './mutation-types';

const orderApi = 'orders';

/* eslint-disable no-unused-vars */

export function fetchOrders({ commit }) {
  return fetch(orderApi)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(FETCH_ORDERS, data);
      }
      return { success, data };
    });
}

export function fetchUserOrders({ commit }, id) {
  return fetch(`users/${id}/${orderApi}`)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_ORDER, data);
      }
      return { success, data };
    });
}

export function saveOrder({ commit }, order) {
  // Relevant payment info {meta: {ref, token, processor}, amount, user_id}
  if (order.meta) {
    const payload = {
      amount: Number(order.total),
      processor: order.meta.processor,
      user_id: order.user_id,
      ref: order.meta.ref
    };
    return savePayment(payload)
      /* eslint-disable no-confusing-arrow */
      .then(({ success, data }) => success === APP_CONSTANTS.OK ? data.token : '')
      .then((token) => {
        order.meta.token = token;
        return save(orderApi, order)
          .then(({ success, data }) => {
            if (success === APP_CONSTANTS.OK) {
              commit(SAVE_ORDER, data);
            }
            return { success, data };
          });
      });
  }

  return save(orderApi, order)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_ORDER, data);
      }
      return { success, data };
    });
}

export function replaceWishListItem({ commit, getters }, { idx, item }) {
  commit(REPLACE_WISHLIST_ITEM, { idx, item, restaurants: getters.getRestaurants });
}

export function deleteWishListItem({ commit, getters }, idx) {
  commit(DELETE_WISHLIST_ITEM, { idx, restaurants: getters.getRestaurants });
}

export function deleteWishList({ commit }) {
  commit(DELETE_WISHLIST);
}

export function saveWish({ commit, getters }, order) {
  commit(SAVE_WISHLIST, { order, restaurants: getters.getRestaurants });
}
