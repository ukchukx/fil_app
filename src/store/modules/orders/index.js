import { round } from '@/filters';
import { isWishListEmpty, computeItemTotals, computeDeliveryFee } from '@/constants';
import * as actions from './actions';
import * as getters from './getters';

import {
  FETCH_ORDERS,
  SAVE_ORDER,
  SAVE_WISHLIST,
  DELETE_WISHLIST,
  DELETE_WISHLIST_ITEM,
  REPLACE_WISHLIST_ITEM,
  REPLACE_WISHLIST
} from './mutation-types';

const initialState = {
  orders: [],
  wishList: {}
};

function computeSubtotal(wishList, restaurants) {
  return computeItemTotals(wishList.items, restaurants);
}

function computeTotal(wishList, restaurants) {
  return round(wishList.subtotal + computeDeliveryFee(wishList, restaurants));
}

function recomputeTotals(wishList, restaurants) {
  wishList.subtotal = computeSubtotal(wishList, restaurants);
  wishList.delivery_fee = computeDeliveryFee(wishList, restaurants);
  wishList.total = computeTotal(wishList, restaurants);
  return wishList;
}

function combineSameItems(wishList) {
  wishList.items = wishList.items
    .reduce((acc, item) => {
      const dup = acc.find(i => item.id === i.id && item.name === i.name && item.price === i.price);
      if (dup) {
        dup.quantity += item.quantity;
        return acc;
      }
      acc.push(item);
      return acc;
    }, []);
  return wishList;
}

/* eslint-disable no-param-reassign */
const mutations = {
  [FETCH_ORDERS](state, orders) {
    state.orders = orders;
  },
  [SAVE_ORDER](state, order) {
    const index = state.orders.findIndex(o => o.id === order.id);

    if (index !== -1) {
      state.orders.splice(index, 1, order);
    } else {
      state.orders.push(order);
    }
  },
  [SAVE_WISHLIST](state, { order, restaurants }) {
    if (isWishListEmpty(state.wishList)) {
      state.wishList = order;
    } else {
      Array.prototype.push.apply(state.wishList.items, order.items);
    }
    state.wishList = combineSameItems(recomputeTotals(state.wishList, restaurants));
  },
  [REPLACE_WISHLIST_ITEM](state, { idx, item, restaurants }) {
    state.wishList.items.splice(idx, 1, item);
    state.wishList = combineSameItems(recomputeTotals(state.wishList, restaurants));
  },
  [REPLACE_WISHLIST](state, { wishList, restaurants }) {
    state.wishList = wishList;
    state.wishList = combineSameItems(recomputeTotals(state.wishList, restaurants));
  },
  [DELETE_WISHLIST](state) {
    state.wishList = { items: [] };
  },
  [DELETE_WISHLIST_ITEM](state, { idx, restaurants }) {
    state.wishList.items.splice(idx, 1);
    if (!state.wishList.items.length) {
      state.wishList = { items: [] };
      return;
    }
    state.wishList = combineSameItems(recomputeTotals(state.wishList, restaurants));
  }
};

export default {
  state: Object.assign({}, initialState),
  actions,
  getters,
  mutations
};
