import { save, fetch, fetchOne, doDelete } from '@/store/common';
import { APP_CONSTANTS, canRefresh } from '@/constants';
import {
  FETCH_ITEMS,
  SAVE_ITEM,
  DELETE_ITEM
} from './mutation-types';

const itemApi = 'items';

/* eslint-disable no-unused-vars */

export function fetchItem({ commit }, id) {
  return fetchOne(itemApi, id)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_ITEM, data);
      }
      return { success, data };
    });
}

export function fetchItems({ commit, getters, dispatch }) { 
  if (!canRefresh(getters.getLastRefreshTimestamp.items)) { 
    return { success: APP_CONSTANTS.OK, data: getters.getItems }; 
  } 
   
  dispatch('registerLastRefreshTimestamp', 'items'); 
  return fetch(itemApi)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(FETCH_ITEMS, data);
      }
      return { success, data };
    });
}

export function saveItem({ commit }, item) {
  return save(itemApi, item)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_ITEM, data);
      }
      return { success, data };
    });
}

export function deleteItem({ commit }, id) {
  return doDelete(`${itemApi}/${id}`)
    .then(({ success }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(DELETE_ITEM, id);
      }
      return { success };
    });
}

