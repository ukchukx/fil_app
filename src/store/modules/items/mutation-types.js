export const FETCH_ITEMS = 'items/fetch_items';
export const SAVE_ITEM = 'items/save_item';
export const DELETE_ITEM = 'items/delete_item';
