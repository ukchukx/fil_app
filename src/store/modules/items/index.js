import * as actions from './actions';
import * as getters from './getters';
import {
  FETCH_ITEMS,
  SAVE_ITEM,
  DELETE_ITEM
} from './mutation-types';

const initialState = {
  items: [],
  lastRefreshed: new Date().getTime()
};

/* eslint-disable no-param-reassign */
const mutations = {
  [FETCH_ITEMS](state, items) {
    state.items = items;
    state.lastRefreshed = new Date().getTime();
  },
  [SAVE_ITEM](state, item) {
    const index = state.items.findIndex(i => i.id === item.id);

    if (index !== -1) {
      state.items.splice(index, 1, item);
    } else {
      state.items.push(item);
    }
  },
  [DELETE_ITEM](state, id) {
    state.items = state.items.filter(f => f.id !== id);
  }
};

export default {
  state: Object.assign({}, initialState),
  actions,
  getters,
  mutations
};
