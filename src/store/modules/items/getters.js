export const getItems = state => state.items;
export const getLastRefreshed = state => state.lastRefreshed;
