import { save, fetch, doDelete, fetchOne } from '@/store/common';
import { APP_CONSTANTS, canRefresh } from '@/constants';
import { FETCH_RESTAURANTS, SAVE_RESTAURANT, DELETE_RESTAURANT } from './mutation-types';

const restaurantApi = 'restaurants';

/* eslint-disable no-unused-vars */

export function fetchRestaurant({ commit }, id) {
  return fetchOne(restaurantApi, id)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_RESTAURANT, data);
      }
      return { success, data };
    });
}

export function fetchRestaurants({ commit, getters, dispatch }) { 
  if (!canRefresh(getters.getLastRefreshTimestamp.restaurants)) { 
    return { success: APP_CONSTANTS.OK, data: getters.getRestaurants }; 
  } 
 
  dispatch('registerLastRefreshTimestamp', 'restaurants'); 
  return fetch(restaurantApi)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(FETCH_RESTAURANTS, data);
      }
      return { success, data };
    });
}

export function saveRestaurant({ commit }, restaurant) {
  return save(restaurantApi, restaurant)
    .then(({ success, data }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(SAVE_RESTAURANT, data);
      }
      return { success, data };
    });
}

export function deleteRestaurant({ commit }, id) {
  return doDelete(`${restaurantApi}/${id}`)
    .then(({ success }) => {
      if (success === APP_CONSTANTS.OK) {
        commit(DELETE_RESTAURANT, id);
      }
      return { success };
    });
}

