import * as actions from './actions';
import * as getters from './getters';
import { FETCH_RESTAURANTS, SAVE_RESTAURANT, DELETE_RESTAURANT } from './mutation-types';

const initialState = {
  restaurants: []
};

/* eslint-disable no-param-reassign */
const mutations = {
  [FETCH_RESTAURANTS](state, restaurants) {
    state.restaurants = restaurants;
  },
  [SAVE_RESTAURANT](state, restaurant) {
    const index = state.restaurants.findIndex(r => r.id === restaurant.id);

    if (index !== -1) {
      state.restaurants.splice(index, 1, restaurant);
    } else {
      state.restaurants.push(restaurant);
    }
  },
  [DELETE_RESTAURANT](state, id) {
    state.restaurants = state.restaurants.filter(r => r.id !== id);
  }
};

export default {
  state: Object.assign({}, initialState),
  actions,
  getters,
  mutations
};
