export const FETCH_RESTAURANTS = 'restaurants/fetch_restaurants';
export const SAVE_RESTAURANT = 'restaurants/save_restaurant';
export const DELETE_RESTAURANT = 'restaurants/delete_restaurant';
