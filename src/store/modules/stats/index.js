import * as actions from './actions';
import * as getters from './getters';

import { FETCH_STATS, REGISTER_TS } from './mutation-types';

const initialState = {
  stats: {
    total_sales: 12334556666,
    total_commissions: 1256666,
    total_delivery_fees: 2343243,
    total_users: 242348920343,
    active_users: 454545454,
    inactive_users: 23424234,
    total_orders: 10000,
    pending_orders: 1980,
    delivered_orders: 8000,
    cancelled_orders: 20
  }, 
  last_request_ts: { 
    tags: 0, 
    users: 0, 
    items: 0, 
    orders: 0, 
    restaurants: 0 
  } 
};

/* eslint-disable no-param-reassign */
const mutations = {
  [FETCH_STATS](state, stats) {
    state.stats = Object.assign(state.stats, stats);
  }, 
  [REGISTER_TS](state, resource) { 
    state.last_request_ts[resource] = new Date().getTime(); 
  }
};

export default {
  state: Object.assign({}, initialState),
  actions,
  getters,
  mutations
};
