import Vue from 'vue';
import { APP_CONSTANTS } from '@/constants';


function fetchOne(url, id) {
  return Vue.http.get(`${url}/${id}`)
    .then(response => Object.assign({}, { success: APP_CONSTANTS.OK, data: response.body.data }),
      error => Object.assign({}, { success: APP_CONSTANTS.FAILED, data: error.body }));
}

function fetch(url) {
  return Vue.http.get(url)
    .then(response => Object.assign({}, { success: APP_CONSTANTS.OK, data: response.body.data }),
      error => Object.assign({}, { success: APP_CONSTANTS.FAILED, data: error.body }));
}

function doDelete(url) {
  return Vue.http.delete(url)
    .then(response => Object.assign({}, { success: APP_CONSTANTS.OK })
    , error => Object.assign({}, { success: APP_CONSTANTS.FAILED }));
}

function save(url, obj) {
  if (obj.id !== '') {
    return Vue.http.put(`${url}/${obj.id}`, obj)
      .then(response => Object.assign({}, { success: APP_CONSTANTS.OK, data: response.body.data })
      , error => Object.assign({}, { success: APP_CONSTANTS.FAILED, data: error.body }));
  }

  return Vue.http.post(url, obj)
    .then(response => Object.assign({}, { success: APP_CONSTANTS.OK, data: response.body.data })
    , error => Object.assign({}, { success: APP_CONSTANTS.FAILED, data: error.body }));
}

function savePayment({ amount, processor, user_id, ref }) {
  return Vue.http.post('payments', { amount, processor, user_id, ref })
    .then(response => Object.assign({}, { data: response.body.data, success: APP_CONSTANTS.OK })
    , error => Object.assign({}, { success: APP_CONSTANTS.FAILED, data: error.body }));
}

export { fetchOne, fetch, save, doDelete, savePayment };
