import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import createPersistedState from 'vuex-persistedstate';
import createMutationsSharer from 'vuex-shared-mutations';

import { apiUrl, inDebugMode } from '@/constants';
import users from './modules/users';
import orders from './modules/orders';
import restaurants from './modules/restaurants';
import tags from './modules/tags';
import items from './modules/items';
import stats from './modules/stats';

Vue.use(Vuex);
Vue.use(VueResource);

const store = new Vuex.Store({
  modules: {
    users,
    orders,
    restaurants,
    tags,
    items,
    stats
  },
  strict: inDebugMode(),
  plugins: [
    createMutationsSharer({
      predicate: (mutation, state) => true
    }),
    createPersistedState({ key: 'fil' })
  ]

});

Vue.http.options.root = apiUrl();
Vue.http.interceptors.push((request, next) => {
  const user = store.getters.getUser; 
  if (user && user.token) { 
    request.headers.set('Authorization', `Bearer ${user.token}`); 
  }
  next();
});

export default store;
