// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import VeeValidate from 'vee-validate';
import App from '@/App';
import router from '@/router';
import store from '@/store';
import { fetchNecessaries } from '@/constants';
import { filterMixin } from '@/filters';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VeeValidate);
Vue.mixin(filterMixin);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});

fetchNecessaries(store);
