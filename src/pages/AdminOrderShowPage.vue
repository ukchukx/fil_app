<template>
  <div class="container w-100 admin-order-show">
    <fil-admin-top-bar></fil-admin-top-bar>
    <div class="row mt-1">
      <div class="col">
        <flash ref="flash"></flash>
      </div>
    </div>
    <form novalidate @submit.stop.prevent="updateOrder">
      <div class="row mt-3">
        <div class="col-md-8 col-sm-12 offset-md-2">
          <div class="row mt-3 ml-4">
            <div class="col">
              <h3 class="text-uppercase">
                ID: {{ order.transaction_id }}
              </h3>
              <small>Order date: {{ orderDate }}</small>
              <small v-if="delivered">&longrightarrow; {{ deliveryDate }}</small>
            </div>
          </div>
          <div class="row mt-3" v-if="!delivered">
            <div class="col">
              <fil-button v-if="canDispatch" text="Dispatch" colour="red"></fil-button>
              <p v-else>Order dispatched to {{ driver }}</p>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col">
              <h3 class="form-control-static text-left">Customer: {{ order.user_id | showName('user', $store) }}</h3>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col">
              <p class="form-control-static text-left">
                Delivery address: {{ order.delivery_street }}, {{ order.delivery_city }}, {{ order.delivery_country }}
              </p>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col">
              <p class="form-control-static text-left">
                Status:
                <span :class="badgeClasses">
                  {{ order.status }}
                </span>
                <!--Temporary control -->
                <a v-if="orderEditable" href="#" @click.stop.prevent="setDelivered"><u>Mark as delivered</u></a>
              </p>
            </div>
            <div class="col" v-if="amountToRefund > 0">
              <p class="form-control-static">
                Amount to refund: {{ amountToRefund | moneyString(order.currency || 'NGN') }}
              </p>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col">
              <table class="table table-hover">
                <thead>
                <tr>
                  <td class="text-uppercase">Item</td>
                  <td class="text-uppercase">Restaurant</td>
                  <td class="text-uppercase">Qty</td>
                  <td class="text-uppercase">Amount</td>
                  <td></td>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, idx) in order.items" :key="`${item.food_item_id}-${item.restaurant_id}`">
                  <td>
                    <p class="h6">{{ item.name }}&ensp;&rightarrow;&ensp;<price :currency="item.currency" :amount="item.price"></price></p>
                    <div v-if="!!item.charges.length">
                      <h6>Extra charges:</h6>
                      <p v-for="(charge, idx) in item.charges" :key="idx" class="mb-0">
                        &ndash; {{ charge.name }}
                        &ensp;&rightarrow;&ensp;<price :currency="item.currency" :amount="charge.price"></price>
                      </p>
                    </div>
                  </td>
                  <td>{{ item.restaurant.name }}</td>
                  <td>
                    <span>{{ item.quantity }}</span>
                    <!--<qty-selector v-else-->
                      <!--@new_qty="changeQuantity"-->
                      <!--class="mb-2 mr-2"-->
                      <!--:initial-qty="item.quantity"-->
                      <!--:min-value="1"-->
                      <!--:rest-id="item.restaurant_id"-->
                      <!--:item-id="item.food_item_id"></qty-selector>-->
                  </td>
                  <td>
                    <h4>
                      <price :currency="order.currency" :amount="itemPrice(item)"></price>
                    </h4>
                  </td>
                  <td>
                    <button v-if="orderEditable" @click.stop.prevent="removeItem(idx)" type="button" class="close" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </td>
                </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td class="text-uppercase">Subtotal</td>
                    <td class="text-uppercase"></td>
                    <td class="text-uppercase">{{ order.subtotal | moneyString(order.currency || 'NGN') }}</td>
                    <td class="text-uppercase"></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td class="text-uppercase">Delivery fee</td>
                    <td class="text-uppercase"></td>
                    <td class="text-uppercase">{{ order.delivery_fee | moneyString(order.currency || 'NGN') }}</td>
                    <td class="text-uppercase"></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td class="text-uppercase">Total</td>
                    <td class="text-uppercase"></td>
                    <td class="text-uppercase">{{ order.total | moneyString(order.currency || 'NGN') }}</td>
                    <td class="text-uppercase"></td>
                    <td></td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-3 mb-4">
        <div class="col">
          <fil-button submit=true text="Update" colour="red"></fil-button>
        </div>
      </div>
    </form>

  </div>
</template>
<script>
  import FilAdminTopBar from '@/components/AdminTopBar';
  import FilButton from '@/components/FilButton';
  import Flash from '@/components/FlashMessage';
  import QtySelector from '@/components/QuantitySelector';
  import Price from '@/components/Price';
  import { APP_CONSTANTS, computeItemTotals, computeDeliveryFee } from '@/constants';
  import moment from 'moment-mini';

  export default {
    name: 'fil-order-show',
    components: {
      FilAdminTopBar,
      FilButton,
      QtySelector,
      Flash,
      Price
    },
    data() {
      return {
        subtotal: 0,
        amountToRefund: 0,
        order: {},
        orderStatus: APP_CONSTANTS.ORDER_STATUS
      };
    },
    mounted() {
      /* eslint-disable no-undef */
      document.title = `Order ${this.order.transaction_id} - FoodILike`;
      this.subtotal = this.order.subtotal;
      this.order = { ...this.$store.getters.getOrders.find(o => o.id === this.$route.params.id) };
    },
    computed: {
      driver() {
        return 'Fix'; // TODO: Implement
      },
      badgeClasses() {
        if (this.order.status === this.orderStatus.PENDING || this.order.status === this.orderStatus.TRANSIT) {
          return 'badge badge-info';
        }
        if (this.order.status === this.orderStatus.DELIVERED) {
          return 'badge badge-success';
        }
        if (this.order.status === this.orderStatus.CANCELLED) {
          return 'badge badge-warning';
        }
        return 'badge';
      },
      canDispatch() {
        // TODO Check if order has been dispatched
        return this.order.status !== APP_CONSTANTS.ORDER_STATUS.DELIVERED ||
          this.order.status !== APP_CONSTANTS.ORDER_STATUS.CANCELLED ||
          this.order.status !== APP_CONSTANTS.ORDER_STATUS.TRANSIT;
      },
      orderEditable() {
        return this.order.status === APP_CONSTANTS.ORDER_STATUS.PENDING || 
          this.order.status === APP_CONSTANTS.ORDER_STATUS.CHECKOUT;
      },
      orderDate() {
        const ts = parseInt(this.order.transaction_id, 16);
        return moment(ts).format('ddd, MMMM Do YYYY, h:mm:ss a');
      },
      deliveryDate() {
        if (this.delivered) return moment(this.order.delivery_date).format('ddd, MMMM Do YYYY, h:mm:ss a');
        return 'Not yet delivered';
      },
      delivered() {
        return this.order.status === APP_CONSTANTS.ORDER_STATUS.DELIVERED;
      },
      restaurants() {
        return this.$store.getters.getRestaurants;
      }
    },
    methods: {
      itemPrice(item) {
        return (item.price + item.charges.reduce((sum, charge) => sum + charge.price, 0)) * item.quantity;
      },
      setDelivered() {
        this.order.status = APP_CONSTANTS.ORDER_STATUS.DELIVERED;
        this.order.delivery_date = new Date().getTime(); // TODO: Move to server
      },
      updateOrder() {
        this.$store.dispatch('saveOrder', this.order)
          .then(({ success, data }) => {
            if (success === APP_CONSTANTS.FAILED) {
              this.showNotice('Could not edit order', true);
            } else {
              this.refund();
              this.showNotice('Order updated', false);
            }
          }, (error) => {
            this.showNotice('Could not edit order', true);
          });
      },
      changeQuantity(itemData) {
        if (this.subtotal === 0) return;

        const item = { ...this.order.items
          .find(i => i.food_item_id === itemData.food_item_id && i.restaurant_id === itemData.restaurant_id) };

        if (item) {
          const idx = this.order.items
            .findIndex(i => i.food_item_id === itemData.food_item_id && i.restaurant_id === itemData.restaurant_id);

          const price = this.$options.filters.round(itemData.quantity * item.food_item_price);
          const subtotal = this.$options.filters.round(
            this.$options.filters.round(price + this.order.subtotal) - item.price);

          if (subtotal <= this.subtotal) { // Only update if order subtotal is not exceeded
            item.quantity = itemData.quantity;
            item.price = this.$options.filters.round(item.quantity * item.food_item_price);

            const itemsCopy = this.order.items.filter((i, index) => index !== idx);
            itemsCopy.push(item);
          }
        }
      },
      calculateTotals() {
        this.order.subtotal = computeItemTotals(this.order.items);
        this.order.delivery_fee = computeDeliveryFee(this.order, this.restaurants);
        this.order.total = this.order.subtotal + this.order.delivery_fee;
      },
      showNotice(msg, isError) {
        if (isError) {
          this.$refs.flash.flashError(msg);
        } else {
          this.$refs.flash.flashSuccess(msg);
        }
      },
      refund() {
        if (this.amountToRefund > 0) {
          this.$store.dispatch('refund', { amount: this.amountToRefund, user_id: this.order.user_id })
            .then(({ success }) => {
              if (success === APP_CONSTANTS.OK) {
                this.amountToRefund = 0;
              }
            });
        }
      },
      removeItem(idx) {
        if (idx >= 0) {
          const [item] = this.order.items.splice(idx, 1);
          this.amountToRefund += this.itemPrice(item);
          this.calculateTotals();
        }
      }
    }
  };
</script>
<style>
  .admin-order-show .red-text {
    color: #E46055 !important;
  }
</style>
