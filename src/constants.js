import { round } from '@/filters';

const APP_CONSTANTS = {
  FAILED: 'failed',
  OK: 'ok',
  ROLES: {
    ADMIN: 'admin',
    USER: 'user',
    DRIVER: 'driver'
  },
  ROUTES: {
    NOT_FOUND: 'four-0-four',
    HOME: 'home',
    CHECKOUT: 'checkout',
    MENU: 'menu',
    ITEM_SHOW: 'item-show',
    REST_SHOW: 'restaurant-show',
    USER_ACCOUNT: 'user-account',
    SIGNIN: 'signin',
    SIGNUP: 'signup',
    FORGOT_PASSWORD: 'forgot-password',
    RESET_PASSWORD: 'reset-password',
    ADMIN_SIGNIN: 'admin-signin',
    ADMIN_HOME: 'admin-home',
    ADMIN_ORDER_LIST: 'admin-orders-list',
    ADMIN_ORDER_SHOW: 'admin-orders-show',
    ADMIN_RESTAURANT_LIST: 'admin-restaurants-list',
    ADMIN_REST_ADD: 'admin-restaurant-add',
    ADMIN_REST_SHOW: 'admin-restaurant-show',
    ADMIN_USER_LIST: 'admin-users-list',
    ADMIN_USER_ADD: 'admin-users-add',
    ADMIN_TAG_LIST: 'admin-tag-list',
    ADMIN_USER_SHOW: 'admin-users-show',
    ADMIN_ITEM_ADD: 'admin-item-add',
    ADMIN_ITEM_SHOW: 'admin-item-show',
    ADMIN_NOTIFS: 'admin-notifs'
  },
  EVENTS: {
    APP_EVENT: 'app',
    ANALYTIC_EVENT: 'anna',
    SIGN_IN: 'user.signin',
    SIGN_OUT: 'user.signout'
  },
  ORDER_STATUS: {
    DELIVERED: 'delivered',
    PENDING: 'pending',
    CHECKOUT: 'checkout',
    CANCELLED: 'cancelled',
    TRANSIT: 'transit'
  },
  EVENT_TYPE: {
    API_CALL: 'api.call',
    API_RESPONSE_ERROR: 'api.response_error'
  }
};

const adminMenu = [
  {
    name: 'Orders',
    location: { name: APP_CONSTANTS.ROUTES.ADMIN_ORDER_LIST }
  },
  {
    name: 'Restaurants',
    location: { name: APP_CONSTANTS.ROUTES.ADMIN_RESTAURANT_LIST }
  },
  {
    name: 'Users',
    location: { name: APP_CONSTANTS.ROUTES.ADMIN_USER_LIST }
  },
  {
    name: 'Tags',
    location: { name: APP_CONSTANTS.ROUTES.ADMIN_TAG_LIST }
  },
  {
    name: 'Menu',
    location: { name: APP_CONSTANTS.ROUTES.MENU }
  }
];

const navMenu = [
  {
    name: 'Sign in',
    signedIn: false,
    location: { name: APP_CONSTANTS.ROUTES.SIGNIN }
  },
  {
    name: 'Sign up',
    signedIn: false,
    location: { name: APP_CONSTANTS.ROUTES.SIGNUP }
  }
];

function inDebugMode() {
  return process.env.NODE_ENV !== 'production';
}

function getUserMenu(user, router) {
  /* eslint-disable no-confusing-arrow */
  return navMenu
    .filter(r => user ? r.signedIn : !r.signedIn)
    .map(r => Object.assign(r, router.resolve(r.location)));
}

function getLogoLink(group) {
  switch (group) {
    case APP_CONSTANTS.ROLES.ADMIN: return { name: APP_CONSTANTS.ROUTES.HOME };
    case APP_CONSTANTS.ROLES.USER: return { name: APP_CONSTANTS.ROUTES.HOME };
    default: return '#';
  }
}

function getAdminMenu() {
  return adminMenu;
}

function isSignedIn(store) {
  const user = store.getters.getUser;
  return !!user.token;
}

function setUpEnvironment(store) {
  switch (store.getters.getUser.role) {
    case 'admin':
      store.dispatch('fetchUsers');
      store.dispatch('fetchOrders');
      break;
    default:
      store.dispatch('fetchUserOrders', store.getters.getUser.id);
  }
  store.dispatch('fetchRestaurants');
  store.dispatch('fetchTags');
  store.dispatch('fetchItems');
}

function fetchNecessaries(store) {
  if (!store) return new Promise((resolve, reject) => resolve(true)); 
  // TODO: We don't need this on auth & 404 pages 
  return Promise.all([
    store.dispatch('fetchRestaurants'),
    store.dispatch('fetchTags'),
    store.dispatch('fetchItems')
  ]);
}

function canRefresh(lastRefreshTimestamp) { 
  // Allow refresh if this is the first time (lastRefreshTimestamp === 0) 
  // or lastRefreshTimestamp is at least 30 seconds past 
  // In dev mode, relax to 10 seconds.
  const interval = inDebugMode() ? 10 : 30;
  return lastRefreshTimestamp === 0 ? true :  
    Math.floor((new Date().getTime() - lastRefreshTimestamp) / 1000) >= interval;
} 

function isRestaurantOpen(restaurant) {
  const today = new Date();
  today.setSeconds(0);
  const day = today.toDateString().toLowerCase().split(' ')[0];
  // const [hour, minutes] = date.toTimeString().split(' ')[0].split(':');
  const [start, close] = restaurant[day].split('-')
    .map(t => Object.assign({}, { h: t.split(':')[0], m: t.split(':')[1] }));

  const startDate = new Date();
  startDate.setHours(start.h);
  startDate.setMinutes(start.m);
  startDate.setSeconds(0);

  const closeDate = new Date();
  closeDate.setHours(close.h);
  closeDate.setMinutes(close.m);
  closeDate.setSeconds(0);

  return today.getTime() >= startDate.getTime() && today.getTime() <= closeDate.getTime();
}

function handleSignout(store) {
  store.dispatch('signout');
}

function isWishListEmpty(wishList) {
  // Vue sticks in __ob__
  const keys = Object.getOwnPropertyNames(wishList);
  return keys.length <= 1 || keys.indexOf('items') === -1 || wishList.items.length === 0;
}

function computeItemTotals(items) {
  const total = items.reduce((sum, item) => {
    const charges = round(item.charges.reduce((acc, charge) => acc + charge.price, 0));
    return round(((item.price + charges) * item.quantity) + sum);
  }, 0);
  return round(total);
}

function computeDeliveryFee(wishList, restaurants) {
  const fee = wishList.items
    .map(item => restaurants.find(r => r.id === item.restaurant.id))
    .filter((rest, index, rests) => rests.indexOf(rest) === index)
    .reduce((sum, rest) => sum + rest.delivery_fee, 0);
  return round(fee);
}

/* eslint-disable no-undef */
function apiHost() {
  return inDebugMode() ?
    `${window.location.protocol}//${window.location.hostname}:4000` : `${window.location.origin}`;
}

function apiUrl() {
  return `${apiHost()}/api/v1`;
}

function getPaystackScriptSrc() {
  return inDebugMode() ? '/static/js/paystack-inline.js' : 'https://js.paystack.co/v1/inline.js';
}

function getSimplepayScriptSrc() {
  return inDebugMode() ? '/static/js/simplepay.js' : 'https://checkout.simplepay.ng/simplepay.js';
}

function getPaystackPublicKey() {
  return inDebugMode() ?
    'pk_test_192937930dd46a859b3b92b77281f3c3183bb090' :
    'pk_live_a5db39aa0ad9e57f7bbd32350405853d5d2fca51';
}

function getSimplePayPublicKey() {
  return inDebugMode() ? 'test_pu_bbef2c6246e94d609c2a5b8868d4769f' :
    'pu_bdeb997bf5ac4806a81959ac3b317a38';
}

function generateTransactionToken() {
  return Math.floor(new Date().getTime()).toString(16).toUpperCase();
}

// function sleepDelegate(ms) {
//   return new Promise(resolve => setTimeout(resolve, ms));
// }

// async function sleep(ms) {
//   await sleepDelegate(ms);
// }

function isMobile() {
  // Let's hope no mobile screens are wider than 500px
  return window.innerWidth < 500;
}

export {
  APP_CONSTANTS,
  getUserMenu,
  getAdminMenu,
  getLogoLink,
  setUpEnvironment,
  isSignedIn,
  handleSignout,
  isWishListEmpty,
  inDebugMode,
  getPaystackScriptSrc,
  getSimplepayScriptSrc,
  getPaystackPublicKey,
  getSimplePayPublicKey,
  generateTransactionToken,
  isRestaurantOpen,
  fetchNecessaries,
  computeItemTotals,
  computeDeliveryFee,
  isMobile,
  apiUrl,
  apiHost,
  canRefresh 
};
