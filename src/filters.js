import moment from 'moment-mini';
import numeral from 'numeral';


function userName(id, store) {
  const user = store.getters.getUsers.find(u => u.id === id);
  return user ? `${user.first_name} ${user.last_name}` : 'user';
}

function restaurantName(id, store) {
  const restaurant = store.getters.getRestaurants.find(r => r.id === id);
  return restaurant ? `${restaurant.name}` : 'restaurant';
}

function tagName(id, store) {
  const tag = store.getters.getTags.find(t => t.id === id);
  return tag ? `${tag.name}` : 'tag';
}

function itemName(store, id) {
  const item = store.getters.getItems.find(f => f.id === id);
  return item ? `${item.name}` : 'item';
}

function showName(id, objectType, store) {
  switch (objectType) {
    case 'user': return userName(id, store);
    case 'restaurant': return restaurantName(id, store);
    case 'tag': return tagName(id, store);
    case 'item': return itemName(id, store);
    default: return id;
  }
}

function toTimeString(timestamp) {
  return moment(timestamp).fromNow();
}

function formatMonetaryValue(value) {
  return `${numeral(value).format('0,0[.]00')}`;
}

function currencySymbol(code) {
  switch (code || 'NGN') {
    case 'NGN': return '₦';
    default: return code;
  }
}

function formatNumber(value) {
  return `${numeral(value).format('0,0')}`;
}

function moneyString(value, currency = 'NGN') {
  return `${currencySymbol(currency)}${formatMonetaryValue(value)}`;
}

function titleCase(str) {
  return str
  .trim()
  .split('')
  .map((ch, idx) => (idx ? ch : ch.toUpperCase()))
  .join('');
}

function tsToPeriod(ts) {
  const hours = new Date(ts).getHours();

  if (hours < 12) return 'morning';
  if (hours < 17) return 'afternoon';
  return 'evening';
}

/**
 * Rounds number to precision
 * @param number
 * @param precision
 * @returns {number}
 */
function round(number, precision = 2) {
  const factor = 10 ** Number(precision);
  const tempNumber = Number(number) * factor;
  const roundedTempNumber = Math.round(tempNumber);
  return roundedTempNumber / factor;
}

const filterMixin = {
  filters: {
    userName,
    restaurantName,
    itemName,
    tagName,
    showName,
    asMoney: formatMonetaryValue,
    asCurrency: currencySymbol,
    round,
    toTimeString,
    tsToPeriod,
    moneyString,
    formatNumber,
    titleCase
  }
};

export {
  round,
  filterMixin
};
