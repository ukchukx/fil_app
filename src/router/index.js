/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import Vue from 'vue';
import Router from 'vue-router';

import { APP_CONSTANTS } from '@/constants';
import store from '@/store';

const AdminSigninPage = resolve => require(['@/pages/AdminSigninPage'], resolve);
const UserSigninPage = resolve => require(['@/pages/UserSigninPage'], resolve);
const UserSignupPage = resolve => require(['@/pages/UserSignupPage'], resolve);
const ForgotPasswordPage = resolve => require(['@/pages/ForgotPasswordPage'], resolve);
const ResetPasswordPage = resolve => require(['@/pages/ResetPasswordPage'], resolve);

const FrontPage = resolve => require(['@/pages/FrontPage'], resolve);
const MenuPage = resolve => require(['@/pages/MenuPage'], resolve);
const ItemPage = resolve => require(['@/pages/ItemPage'], resolve);
const RestaurantPage = resolve => require(['@/pages/RestaurantPage'], resolve);
const UserAccountPage = resolve => require(['@/pages/UserAccountPage'], resolve);
const CheckoutPage = resolve => require(['@/pages/CheckoutPage'], resolve);

const AdminDashboard = resolve => require(['@/pages/AdminDashboard'], resolve);
const AdminOrderListPage = resolve => require(['@/pages/AdminOrderListPage'], resolve);
const AdminOrderShowPage = resolve => require(['@/pages/AdminOrderShowPage'], resolve);
const AdminRestaurantListPage = resolve => require(['@/pages/AdminRestaurantListPage'], resolve);
const AdminUserListPage = resolve => require(['@/pages/AdminUserListPage'], resolve);
const AdminTagListPage = resolve => require(['@/pages/AdminTagListPage'], resolve);
const AdminRestaurantShowPage = resolve => require(['@/pages/AdminRestaurantShowPage'], resolve);
const AdminUserShowPage = resolve => require(['@/pages/AdminUserShowPage'], resolve);
const AdminItemShowPage = resolve => require(['@/pages/AdminItemShowPage'], resolve);
const AdminRestaurantAddPage = resolve => require(['@/pages/AdminRestaurantAddPage'], resolve);
const AdminUserAddPage = resolve => require(['@/pages/AdminUserAddPage'], resolve);
const AdminItemAddPage = resolve => require(['@/pages/AdminItemAddPage'], resolve);

const FourOFour = resolve => require(['@/pages/FourOFour'], resolve);


Vue.use(Router);

const routes = [
  {
    path: '/bo/signin',
    name: APP_CONSTANTS.ROUTES.ADMIN_SIGNIN,
    component: AdminSigninPage,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: '/bo/dashboard',
    name: APP_CONSTANTS.ROUTES.ADMIN_HOME,
    component: AdminDashboard,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/orders',
    name: APP_CONSTANTS.ROUTES.ADMIN_ORDER_LIST,
    component: AdminOrderListPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/o/:id',
    name: APP_CONSTANTS.ROUTES.ADMIN_ORDER_SHOW,
    component: AdminOrderShowPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/tags',
    name: APP_CONSTANTS.ROUTES.ADMIN_TAG_LIST,
    component: AdminTagListPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/f/:id',
    name: APP_CONSTANTS.ROUTES.ADMIN_ITEM_SHOW,
    component: AdminItemShowPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/r/:restaurant_id/add-item',
    name: APP_CONSTANTS.ROUTES.ADMIN_ITEM_ADD,
    component: AdminItemAddPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/notifications',
    name: APP_CONSTANTS.ROUTES.ADMIN_NOTIFS,
    component: AdminOrderListPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/users',
    name: APP_CONSTANTS.ROUTES.ADMIN_USER_LIST,
    component: AdminUserListPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/add-user',
    name: APP_CONSTANTS.ROUTES.ADMIN_USER_ADD,
    component: AdminUserAddPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/u/:id',
    name: APP_CONSTANTS.ROUTES.ADMIN_USER_SHOW,
    component: AdminUserShowPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/restaurants',
    name: APP_CONSTANTS.ROUTES.ADMIN_RESTAURANT_LIST,
    component: AdminRestaurantListPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/add-restaurant',
    name: APP_CONSTANTS.ROUTES.ADMIN_REST_ADD,
    component: AdminRestaurantAddPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/bo/r/:id',
    name: APP_CONSTANTS.ROUTES.ADMIN_REST_SHOW,
    component: AdminRestaurantShowPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/',
    name: APP_CONSTANTS.ROUTES.HOME,
    component: FrontPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/menu',
    name: APP_CONSTANTS.ROUTES.MENU,
    component: MenuPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/f/:id',
    name: APP_CONSTANTS.ROUTES.ITEM_SHOW,
    component: ItemPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/r/:id',
    name: APP_CONSTANTS.ROUTES.REST_SHOW,
    component: RestaurantPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/checkout',
    name: APP_CONSTANTS.ROUTES.CHECKOUT,
    component: CheckoutPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/me',
    name: APP_CONSTANTS.ROUTES.USER_ACCOUNT,
    component: UserAccountPage,
    meta: {
      requiresAuth: true,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/signin',
    name: APP_CONSTANTS.ROUTES.SIGNIN,
    component: UserSigninPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER
      ]
    }
  },
  {
    path: '/signup',
    name: APP_CONSTANTS.ROUTES.SIGNUP,
    component: UserSignupPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER
      ]
    }
  },
  {
    path: '/forgot-password',
    name: APP_CONSTANTS.ROUTES.FORGOT_PASSWORD,
    component: ForgotPasswordPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    // Important! If this url is changed, it should be changed on the server in the forgot password controller
    path: '/reset-password/:token',
    name: APP_CONSTANTS.ROUTES.RESET_PASSWORD,
    component: ResetPasswordPage,
    meta: {
      requiresAuth: false,
      roles: [
        APP_CONSTANTS.ROLES.DRIVER,
        APP_CONSTANTS.ROLES.USER,
        APP_CONSTANTS.ROLES.ADMIN
      ]
    }
  },
  {
    path: '/*',
    redirect: { name: APP_CONSTANTS.ROUTES.NOT_FOUND }
  },
  {
    path: '/oops',
    name: APP_CONSTANTS.ROUTES.NOT_FOUND,
    component: FourOFour,
    meta: {
      requiresAuth: false
    }
  }
];

const router = new Router({
  routes,
  mode: 'history'
});

// Authenticate before accessing protected route
/* eslint-disable no-prototype-builtins */
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const user = store.getters.getUser;

    if (!user.token) { // Not authenticated? Send user to login
      if (to.path.indexOf('/bo/') !== -1) { // Send to admin sign in
        next({
          name: APP_CONSTANTS.ROUTES.ADMIN_SIGNIN,
          query: { to: to.fullPath }
        });
      } else {
        next({
          name: APP_CONSTANTS.ROUTES.SIGNIN,
          query: { to: to.fullPath }
        });
      }
    } else if (!user || to.meta.roles.indexOf(user.role) === -1) { // wrong role? return to sender
      switch (user.role) {
        case 'admin':
          next({ name: APP_CONSTANTS.ROUTES.ADMIN_HOME });
          break;
        default:
          next({ name: APP_CONSTANTS.ROUTES.USER_ACCOUNT });
      }
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
