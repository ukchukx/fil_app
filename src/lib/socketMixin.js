import { Socket } from 'phoenix-socket';
import { apiHost, APP_CONSTANTS, inDebugMode } from '@/constants';
import { showNotif, playNotifNote } from '@/lib/notifs';

/* eslint-disable no-console */
const socketMixin = {
  mounted() {
    const debug = inDebugMode();
    const user = this.$store.getters.getUser;

    if (user.role === 'admin') {
      const socket = new Socket(`${apiHost().replace('http', 'ws')}/socket`, {
        params: { token: user.token },
        logger: (kind, msg, data) => {
          if (debug) console.log(`${kind}: ${msg}::`, data);
        }
      });

      socket.connect();
      socket.onError(ev => console.error('socket', ev));
      if (debug) socket.onClose(ev => console.info('socket', ev));

      const adminChannel = socket.channel('rooms:admin', {});
      adminChannel.join()
      .receive('ok', resp => console.info('joined the admin channel', resp))
      .receive('error', reason => console.error('join failed', reason));

      adminChannel.onError(ev => console.error('channel', ev));
      if (debug) adminChannel.onClose(ev => console.info('channel', ev));

      adminChannel.on('new_user', (data) => {
        playNotifNote();
        const msg = `New user: ${data.email}`;
        const link = this.$router
          .resolve({ name: APP_CONSTANTS.ROUTES.ADMIN_USER_SHOW, params: { id: data.id } }).href;
        showNotif({ msg, link });
      });

      adminChannel.on('new_order', (data) => {
        playNotifNote();
        const msg = `New order: ${data.transaction_id}`;
        const link = this.$router
          .resolve({ name: APP_CONSTANTS.ROUTES.ADMIN_ORDER_SHOW, params: { id: data.id } }).href;
        showNotif({ msg, link });
      });
    }
  }
};

export {
  socketMixin
};

