/* eslint-disable no-undef */
function showNotif({ msg = '', link = '', title = 'Notification' }) {
  if (!window.Notification) {
    const errMsg = 'This browser does not support desktop notifications';
    // eslint-disable-next-line no-alert
    alert(errMsg);
    return;
  }

  Notification.requestPermission().then(() => {
    const notif = new Notification(title, {
      icon: `${window.location.origin}/static/logos/FoodILike-mark-red.svg`,
      body: msg
    });
    notif.onclick = () => {
      if (link) window.open(link);
    };
  });
}

function playNotifNote(selector = '#note') {
  const el = document.querySelector(selector);
  if (el) el.play();
}

export {
  showNotif,
  playNotifNote
};
