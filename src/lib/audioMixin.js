/* eslint-disable no-undef */
import { playNotifNote } from '@/lib/notifs';

const audioMixin = {
  mounted() {
    if (document.querySelector('#note')) return;
    const audio = document.createElement('audio');
    audio.id = 'note';
    const source = document.createElement('source');
    source.src = '/static/sounds/gets-in-the-way.mp3';
    audio.appendChild(source);
    document.body.appendChild(audio);
  },
  methods: {
    playNotifNote
  }
};

export {
  audioMixin
};

