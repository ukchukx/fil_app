FROM nginx
COPY dist /var/www/html
COPY docker/nginx.conf /etc/nginx
RUN rm /etc/nginx/conf.d/default.conf && \
  mkdir -p /var/log/fil_app && \
  touch /var/log/fil_app/nginx.access.log /var/log/fil_app/nginx.error.log
